package com.microservice.course_service.service;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Service;

import com.microservice.course_service.model.Course;
import com.microservice.course_service.repository.CourseRepository;

@Service
public class CourseServiceImpl implements CourseService {
	@Autowired
	private CourseRepository courseRepository;

	@Override
	public List<Course> getAllCourse() {
		// TODO Auto-generated method stub
		return courseRepository.findAll();
	}

	@Override
	public Course getCourseById(int course_id) {
		// TODO Auto-generated method stub
		return courseRepository.findById(course_id)
				.orElseThrow(() -> new RuntimeException("Not found course had id: " + course_id));
	}

	@Override
	public Course createCourse(Course course) {
		// TODO Auto-generated method stub
		return courseRepository.save(course);
	}

	@Override
	public Course updateCourse(int course_id, Course updateCourse) {
		// TODO Auto-generated method stub
		Course course = courseRepository.findById(course_id).orElseThrow(() -> new RuntimeException("Not found course has id: " + course_id));

		course.setCourse_name(updateCourse.getCourse_name());
		course.setCourse_lesson(updateCourse.getCourse_lesson());
		course.setStart_date(updateCourse.getStart_date());
		course.setEnd_date(updateCourse.getEnd_date());

		return courseRepository.save(course);
	}

	@Override
	public void deleteCourse(int course_id) {
		// TODO Auto-generated method stub
		
	}
	
}