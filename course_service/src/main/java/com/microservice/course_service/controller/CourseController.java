package com.microservice.course_service.controller;


import com.microservice.course_service.model.Course;
import com.microservice.course_service.service.CourseService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.*;

import java.util.List;
@RestController
@RequestMapping("/api/v1/course")

public class CourseController {

	@Autowired
	private CourseService courseService;
	
	@GetMapping("")
	public ResponseEntity<List<Course>> getAllCourses(){
		List<Course> course = courseService.getAllCourse();
		return ResponseEntity.ok(course);
	}
	
	@GetMapping("/{course_id}")
	public ResponseEntity<Course> findCourseById(@PathVariable("course_id") Integer course_id){
		Course course = courseService.getCourseById(course_id);

		return ResponseEntity.ok(course);
	}
	
	@PostMapping("")
	public ResponseEntity<Course> createCourse(@RequestBody Course course){
		Course createCourse = courseService.createCourse(course);
		return ResponseEntity.ok(createCourse);
	}

	@PutMapping("/{course_id}")
	public ResponseEntity<Course> updateCourse(@PathVariable("course_id") Integer course_id, @RequestBody Course course, BindingResult errors){
		if(errors.hasErrors()){
			return new ResponseEntity<>(HttpStatus.NOT_FOUND);
		}
		courseService.updateCourse(course_id, course);
		return new ResponseEntity<>(HttpStatus.OK);
	}

	@DeleteMapping("/{course_id}")
	public ResponseEntity<Course> deleteCourse(@PathVariable("course_id") Integer course_id, BindingResult errors){
		if(errors.hasErrors()){
			return new ResponseEntity<>(HttpStatus.BAD_REQUEST);
		}
		courseService.deleteCourse(course_id);
		return new ResponseEntity<>(HttpStatus.OK);
	}
}
