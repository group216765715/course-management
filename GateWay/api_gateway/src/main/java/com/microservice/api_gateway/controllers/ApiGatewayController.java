package com.microservice.api_gateway.controllers;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpEntity;
import org.springframework.http.HttpMethod;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.client.RestTemplate;

@RestController
public class ApiGatewayController {

    @Autowired
    private RestTemplate restTemplate;

    private final String COURSE_SERVICE_URL = "http://localhost:8082/api/v1/course";
    private final String EVALUATE_SERVICE_URL = "http://localhost:8083/api/v1/evaluate";
    private final String CART_SERVICE_URL = "http://localhost:8085/api/v1/cart";
    private final String WISHLIST_SERVICE_URL = "http://localhost:8084/api/v1/wishlist";
    private final String USER_SERVICE_URL = "http://localhost:8081/api/v1/";

    // api for course
    @GetMapping("/course")
    public ResponseEntity<String> getAllCourses() {
        return restTemplate.getForEntity(COURSE_SERVICE_URL, String.class);
    }

    @GetMapping("/course/{course_id}")
    public ResponseEntity<String> getCourseById(@PathVariable("course_id") Integer course_id) {
        return restTemplate.getForEntity(COURSE_SERVICE_URL + "/" + course_id, String.class);
    }

    @PostMapping("/course")
    public ResponseEntity<String> createCourse(@RequestBody String course) {
        return restTemplate.postForEntity(COURSE_SERVICE_URL, course, String.class);
    }

    @PutMapping("/course/{course_id}")
    public ResponseEntity<String> updateCourse(@PathVariable("course_id") Integer course_id, @RequestBody String course) {
        return restTemplate.exchange(COURSE_SERVICE_URL + "/" + course_id, HttpMethod.PUT, new HttpEntity<>(course), String.class);
    }

    @DeleteMapping("/course/{course_id}")
    public ResponseEntity<String> deleteCourse(@PathVariable("course_id") Integer course_id) {
        return restTemplate.exchange(COURSE_SERVICE_URL + "/" + course_id, HttpMethod.DELETE, null, String.class);
    }

    // evaluate
    @PostMapping("/evaluate")
    public ResponseEntity<String> evaluateCourse(@RequestBody String evaluateRequest) {
        return restTemplate.postForEntity(EVALUATE_SERVICE_URL + "/evaluate", evaluateRequest, String.class);
    }

    @PostMapping("/cart/add")
    public ResponseEntity<String> addToCart(@RequestBody String cartItem) {
        return restTemplate.postForEntity(CART_SERVICE_URL + "/cart/add", cartItem, String.class);
    }

    @PostMapping("/wishlist/add")
    public ResponseEntity<String> addToWishlist(@RequestBody String wishlistItem) {
        return restTemplate.postForEntity(WISHLIST_SERVICE_URL + "/wishlist/add", wishlistItem, String.class);
    }

    @GetMapping("/user/{id}")
    public ResponseEntity<String> getUser(@PathVariable String id) {
        return restTemplate.getForEntity(USER_SERVICE_URL + "/user/" + id, String.class);
    }
}
