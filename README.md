# COURSE MANAGEMENT MICROSERVICE PROJECT

----

## 1. Cloud Service

    - Api Gate Way Service
    - Eureka Discovery Server Service (http://localhost:8761)
    - Eureka Discovery Client Service
    - Spring Cloud Service
    - Spring Cloud Circuit Breaker
    - Spring Cloud Config

## 2. Service

    - User Service (http://localhost:8081/api/v1)
    - Course Service (http://localhost:8082/api/v1/course)
    - Evaluate Service (http://localhost:8083/api/v1/evaluate)
    - Wishlist Service (http://localhost:8084/api/v1/wishlist)
    - Cart Service (http://localhost:8085/api/v1/cart) 
