package com.microservice.course_service.controller;


import com.microservice.course_service.model.Course;
import com.microservice.course_service.service.CourseService;
import lombok.extern.log4j.Log4j2;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.*;

import java.util.List;
@RestController
@RequestMapping("/api/v1/course")
@Log4j2
public class CourseController {

	@Autowired
	private CourseService courseService;


	
	@GetMapping("")

	public ResponseEntity<List<Course>> getAllCourses(){
		List<Course> course = courseService.getAllCourse();
		return new ResponseEntity<>(course, HttpStatus.OK);
	}

	@GetMapping("/{course_id}")
	public ResponseEntity<Course> findCourseById(@PathVariable("course_id") Integer course_id) throws InterruptedException {
		int retryCount = 0;
		Course course = null;
		while (retryCount < 3) {
			course = courseService.getCourseById(course_id);
			if (course != null) {
				log.info("Get course by id " + course_id + " success!");
				return new ResponseEntity<>(course, HttpStatus.OK);
			}
			Thread.sleep(5000); // Wait for 3 seconds before retrying
			retryCount++;
		}
		log.error("Failed to get course by id " + course_id + " after retrying.");
		return new ResponseEntity<>(HttpStatus.NOT_FOUND);
	}


	@PostMapping("")
	public ResponseEntity<Course> createCourse(@RequestBody Course course){
		Course createCourse = courseService.createCourse(course);
		log.info("Created!");
		return new ResponseEntity<Course>(createCourse, HttpStatus.CREATED);
	}

	@PutMapping("/{course_id}")
	public ResponseEntity<Course> updateCourse(@PathVariable("course_id") Integer course_id, @RequestBody Course course, BindingResult errors){
		if(errors.hasErrors()){
			log.error("Not found!");
			return new ResponseEntity<Course>(HttpStatus.NOT_FOUND);
		}
		courseService.updateCourse(course_id, course);
		log.info("Updated!");
		return new ResponseEntity<>(HttpStatus.OK);
	}

	@DeleteMapping("/{course_id}")
	public ResponseEntity<Course> deleteCourse(@PathVariable("course_id") Integer course_id, BindingResult errors){
		if(errors.hasErrors()){
			log.error("Not found!");
			return new ResponseEntity<>(HttpStatus.BAD_REQUEST);
		}
		courseService.deleteCourse(course_id);
		log.info("Deleted!");
		return new ResponseEntity<>(HttpStatus.OK);
	}
}
