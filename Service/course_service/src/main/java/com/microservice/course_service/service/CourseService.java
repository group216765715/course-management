package com.microservice.course_service.service;

import com.microservice.course_service.model.Course;
import org.springframework.stereotype.Service;

import java.util.List;

@Service

public interface CourseService {
	
	List<Course> getAllCourse();
	 
	Course getCourseById(int course_id);
	
	Course createCourse(Course course);
	
	Course updateCourse(int course_id, Course course);
	
	void deleteCourse(int course_id);
}
