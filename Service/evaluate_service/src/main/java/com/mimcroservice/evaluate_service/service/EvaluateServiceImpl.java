package com.mimcroservice.evaluate_service.service;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.mimcroservice.evaluate_service.model.Evaluate;
import com.mimcroservice.evaluate_service.repository.EvaluateRepository;


import java.util.Date;
import java.util.List;


@Service
public class EvaluateServiceImpl implements EvaluateService{
	
	@Autowired
    private EvaluateRepository evaluateRepository;

    @Override
    public List<Evaluate> getAllEvaluations() {
        return evaluateRepository.findAll();
    }

    @Override
    public Evaluate createEvaluate(Evaluate evaluate) {
        evaluate.setCreate_at(new Date());
        return evaluateRepository.save(evaluate);
    }



    @Override
    public Evaluate getEvaluationById(int evaluate_id) {
        return evaluateRepository.findById(evaluate_id).orElseThrow(() -> new RuntimeException("Not found evaluate as id: " + evaluate_id));
    }

    @Override
    public Evaluate updateEvaluation(int evaluate_id, Evaluate updateEvaluate) {
        Evaluate evaluate = evaluateRepository.findById(evaluate_id).orElseThrow(() -> new RuntimeException("Not found evaluate as id: " + evaluate_id));
        updateEvaluate.setCreate_at(new Date());
        evaluate.setRating(updateEvaluate.getRating());
        evaluate.setFeedback(updateEvaluate.getFeedback());
        evaluate.setCreate_at(updateEvaluate.getCreate_at());
        return evaluateRepository.save(evaluate);
    }

    @Override
    public void deleteEvaluation(int evaluateId) {
        if(evaluateRepository.existsById(evaluateId)){
            evaluateRepository.deleteById(evaluateId);
        } else {
            throw new RuntimeException("Evaluate not found with id: " + evaluateId);
        }
    }
}
