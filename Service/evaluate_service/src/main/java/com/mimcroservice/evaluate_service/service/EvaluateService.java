package com.mimcroservice.evaluate_service.service;

import java.util.List;

import org.springframework.stereotype.Service;

import com.mimcroservice.evaluate_service.model.Evaluate;

@Service
public interface EvaluateService {

    List<Evaluate> getAllEvaluations();
	Evaluate createEvaluate(Evaluate evaluate);

    Evaluate getEvaluationById(int evaluate_id);
    Evaluate updateEvaluation(int evaluate_id, Evaluate updateEvaluate);
    void deleteEvaluation(int evaluate_id);
}
