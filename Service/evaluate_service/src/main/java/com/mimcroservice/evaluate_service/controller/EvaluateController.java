package com.mimcroservice.evaluate_service.controller;

import com.mimcroservice.evaluate_service.model.Evaluate;
import com.mimcroservice.evaluate_service.service.EvaluateService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.*;
import java.util.List;

@RestController
@RequestMapping("/api/v1/evaluate")
public class EvaluateController {

    @Autowired
    private EvaluateService evaluateService;

    @PostMapping("")
    public ResponseEntity<Evaluate> createEvaluate(@RequestBody Evaluate evaluate, BindingResult errors){
        if(errors.hasErrors()) {
            return new ResponseEntity<Evaluate>(HttpStatus.BAD_REQUEST);
        }
        evaluateService.createEvaluate(evaluate);
        return new ResponseEntity<Evaluate>(evaluate, HttpStatus.OK);
    }

    // api get all evaluates
    @GetMapping("")
    public List<Evaluate> getAllEvaluations() {
        return evaluateService.getAllEvaluations();
    }

    @GetMapping("/{evaluate_id}")
    public Evaluate getEvaluationById(@PathVariable int evaluate_id) {
        return evaluateService.getEvaluationById(evaluate_id);
    }

    @PutMapping("/{evaluate_id}")
    public ResponseEntity<Evaluate> updateEvaluation(@PathVariable int evaluate_id, @RequestBody Evaluate updateEvaluate) {
        Evaluate updatedEvaluate = evaluateService.updateEvaluation(evaluate_id, updateEvaluate);
        return ResponseEntity.ok().body(updatedEvaluate);
    }

    @DeleteMapping("/{evaluate_id}")
    public ResponseEntity<Void> deleteEvaluation(@PathVariable int evaluate_id) {
        try {
            evaluateService.deleteEvaluation(evaluate_id);
            return new ResponseEntity<>(HttpStatus.OK);
        } catch (RuntimeException e) {
            return new ResponseEntity<>(HttpStatus.NOT_FOUND);
        }
    }
}
