package com.mimcroservice.evaluate_service.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import com.mimcroservice.evaluate_service.model.Evaluate;

@Repository
public interface EvaluateRepository extends JpaRepository<Evaluate, Integer>{

}
