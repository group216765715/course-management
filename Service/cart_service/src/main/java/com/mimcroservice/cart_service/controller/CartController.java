package com.mimcroservice.cart_service.controller;

import com.mimcroservice.cart_service.model.Cart;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import com.mimcroservice.cart_service.service.CartService;

import java.util.Map;


@RestController
@RequestMapping("/api/v1/cart")
public class CartController {

    @Autowired
    private CartService cartService;

    @PostMapping
    public ResponseEntity<Void> createItem(@RequestBody Cart cart) {
        cartService.saveItem(cart);
        return ResponseEntity.status(HttpStatus.CREATED).build();
    }

    @GetMapping("/{itemId}")
    public ResponseEntity<Cart> getItemById(@PathVariable int itemId) {
        Cart item = cartService.getItemById(itemId);
        return item != null ? ResponseEntity.ok(item) : ResponseEntity.notFound().build();
    }

    @PutMapping("/{itemId}")
    public ResponseEntity<Void> updateItem(@PathVariable int itemId, @RequestBody Cart item) {
        item.setItem_id(itemId); // Ensure itemId is set correctly
        cartService.updateItem(item);
        return ResponseEntity.noContent().build();
    }

    @DeleteMapping("/{itemId}")
    public ResponseEntity<Void> deleteItem(@PathVariable int itemId) {
        cartService.deleteItem(itemId);
        return ResponseEntity.noContent().build();
    }
}