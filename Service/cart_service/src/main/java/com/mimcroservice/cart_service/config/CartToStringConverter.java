package com.mimcroservice.cart_service.config;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.mimcroservice.cart_service.model.Cart;
import org.springframework.core.convert.converter.Converter;
import org.springframework.stereotype.Component;

@Component
public class CartToStringConverter implements Converter<Cart, String> {

    private final ObjectMapper objectMapper;

    public CartToStringConverter(ObjectMapper objectMapper) {
        this.objectMapper = objectMapper;
    }

    @Override
    public String convert(Cart cart) {
        try {
            return objectMapper.writeValueAsString(cart);
        } catch (Exception e) {
            throw new RuntimeException("Error converting Cart to String", e);
        }
    }
}