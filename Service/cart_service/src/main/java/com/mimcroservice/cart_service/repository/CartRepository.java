package com.mimcroservice.cart_service.repository;

import com.fasterxml.jackson.core.JsonProcessingException;
import jakarta.annotation.PostConstruct;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.redis.core.HashOperations;
import org.springframework.data.redis.core.RedisTemplate;
import org.springframework.stereotype.Repository;

import com.mimcroservice.cart_service.model.Cart;
import java.util.Map;

public interface CartRepository {

    Cart findById(int id) throws JsonProcessingException;
    void save(Cart cart) throws JsonProcessingException;
    void update(Cart cart) throws JsonProcessingException;
    void delete(int id);
}