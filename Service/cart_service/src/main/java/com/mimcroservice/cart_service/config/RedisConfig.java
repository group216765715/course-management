package com.mimcroservice.cart_service.config;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.mimcroservice.cart_service.model.Cart;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.data.redis.connection.RedisConnectionFactory;
import org.springframework.data.redis.core.RedisTemplate;
import org.springframework.data.redis.core.convert.RedisCustomConversions;
import org.springframework.data.redis.serializer.GenericToStringSerializer;
import org.springframework.data.redis.serializer.StringRedisSerializer;
import org.springframework.core.convert.converter.Converter;
import java.util.List;
@Configuration
public class RedisConfig {

//	@Bean
//	public RedisTemplate<String, Cart> redisTemplate(RedisConnectionFactory connectionFactory) {
//		RedisTemplate<String, Cart> redisTemplate = new RedisTemplate<>();
//		redisTemplate.setConnectionFactory(connectionFactory);
//		redisTemplate.setKeySerializer(new StringRedisSerializer());
//		redisTemplate.setValueSerializer(new GenericToStringSerializer<>(Cart.class));
//		return redisTemplate;
//	}

	@Bean
	public RedisTemplate<String, Cart> redisTemplate(RedisConnectionFactory connectionFactory) {
		RedisTemplate<String, Cart> redisTemplate = new RedisTemplate<>();
		redisTemplate.setConnectionFactory(connectionFactory);
		redisTemplate.setKeySerializer(new StringRedisSerializer());
		// Set the custom converter
		redisTemplate.setValueSerializer(new GenericToStringSerializer<>(Cart.class));
		return redisTemplate;
	}

	@Bean
	public RedisCustomConversions redisCustomConversions(List<Converter<?, ?>> converters) {
		return new RedisCustomConversions(converters);
	}

	// Add your custom converter to the Spring context
	@Bean
	public CartToStringConverter cartToStringConverter(ObjectMapper objectMapper) {
		return new CartToStringConverter(objectMapper);
	}
}