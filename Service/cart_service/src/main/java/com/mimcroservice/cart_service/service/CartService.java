package com.mimcroservice.cart_service.service;


import com.fasterxml.jackson.core.JsonProcessingException;
import com.mimcroservice.cart_service.model.Cart;
import com.mimcroservice.cart_service.repository.CartRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.redis.core.RedisTemplate;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.Map;

@Service
public class CartService {

    private static final String REDIS_KEY_PREFIX = "item:";

    @Autowired
    private RedisTemplate<String, Cart> redisTemplate;

    public void saveItem(Cart cart) {
        redisTemplate.opsForValue().set(REDIS_KEY_PREFIX + cart.getItem_id(), cart);
    }

    public Cart getItemById(int itemId) {
        return redisTemplate.opsForValue().get(REDIS_KEY_PREFIX + itemId);
    }

    public void updateItem(Cart cart) {
        redisTemplate.opsForValue().set(REDIS_KEY_PREFIX + cart.getItem_id(), cart);
    }

    public void deleteItem(int itemId) {
        redisTemplate.delete(REDIS_KEY_PREFIX + itemId);
    }
}